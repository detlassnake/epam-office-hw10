package ua.epam.hwTen.service;

import ua.epam.hwTen.dao.ProjectsDao;
import ua.epam.hwTen.dto.ProjectDto;
import ua.epam.hwTen.dto.DeveloperDto;
import ua.epam.hwTen.entity.Mentor;
import ua.epam.hwTen.entity.Project;
import ua.epam.hwTen.entity.Developer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DeveloperAssemblerImpl implements DeveloperAssembler {

    @Autowired
    private ProjectsDao projectsDao;

    @Override
    public Developer assemble(DeveloperDto dto) {
        Developer developer = new Developer();
        developer.setId(dto.getId());
        developer.setName(dto.getName());
        developer.setPassportNum(dto.getPassportNum());

        Map<Integer, Mentor> mentorById = getProjects(dto);

        for (ProjectDto projectDto : dto.getProjects()) {
            Project project = assemble(projectDto);
            project.setDeveloper(developer);

            Mentor mentor = mentorById.get(projectDto.getMentorId());
            if (null == mentor) {
                throw new IllegalArgumentException("Project with id " + projectDto.getMentorId() + " does not exist");
            }
            project.setMentor(mentor);

            developer.getProjects().add(project);
        }

        return developer;
    }

    private Map<Integer, Mentor> getProjects(DeveloperDto dto) {
        List<Integer> projectIds = dto.getProjects().stream().map(ProjectDto::getMentorId).collect(Collectors.toList());
        List<Mentor> mentorList = projectsDao.getByIds(projectIds);
        return mentorList.stream().collect(Collectors.toMap(Mentor::getId, Function.identity()));
    }

    @Override
    public DeveloperDto assemble(Developer entity) {
        DeveloperDto dto = new DeveloperDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPassportNum(entity.getPassportNum());

        List<ProjectDto> projects = entity.getProjects()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList());

        dto.setProjects(projects);
        return dto;
    }

    private Project assemble(ProjectDto dto) {
        Project project = new Project();
        project.setId(dto.getId());
        project.setDescription(dto.getDescription());
        return project;
    }

    private ProjectDto assemble(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setDescription(project.getDescription());
        projectDto.setMentorId(project.getMentor().getId());
        return projectDto;
    }
}
