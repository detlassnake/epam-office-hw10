package ua.epam.hwTen.service;

import ua.epam.hwTen.dto.DeveloperDto;
import ua.epam.hwTen.entity.Developer;

public interface DeveloperAssembler {
    Developer assemble(DeveloperDto dto);
    DeveloperDto assemble(Developer entity);
}
