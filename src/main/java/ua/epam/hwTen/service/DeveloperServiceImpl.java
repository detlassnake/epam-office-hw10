package ua.epam.hwTen.service;

import ua.epam.hwTen.dao.DeveloperDao;
import ua.epam.hwTen.dto.DeveloperDto;
import ua.epam.hwTen.entity.Project;
import ua.epam.hwTen.entity.Developer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    private DeveloperDao developerDao;
    @Autowired
    private DeveloperAssembler developerAssembler;

    @Override
    public DeveloperDto create(DeveloperDto dto) {
        Developer developer = developerAssembler.assemble(dto);
        developerDao.save(developer);
        DeveloperDto result = developerAssembler.assemble(developer);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public DeveloperDto get(String passportNumber) {
        Developer developer = developerDao.get(passportNumber);
        return developerAssembler.assemble(developer);
    }

    @Override
    @Transactional
    public DeveloperDto update(DeveloperDto dto) {
        Developer entity = developerDao.get(dto.getId());
        Developer updatedEntity = developerAssembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return developerAssembler.assemble(entity);
    }

    private void performUpdate(Developer persistentEntity, Developer newEntity) {
        persistentEntity.setName(newEntity.getName());
        persistentEntity.setPassportNum(newEntity.getPassportNum());

        updateProjects(persistentEntity.getProjects(), newEntity.getProjects());
    }

    private void updateProjects(List<Project> persistentProjects, List<Project> newProjects) {
        Map<Integer, Project> stillExistentProjects = newProjects
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Project::getId, Function.identity()));

        List<Project> projectsToAdd = newProjects
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Project> persistentIterator = persistentProjects.iterator();
        while (persistentIterator.hasNext()) {
            Project persistentProject = persistentIterator.next();
            if (stillExistentProjects.containsKey(persistentProject.getId())) {
                Project updatedProject = stillExistentProjects.get(persistentProject.getId());
                updateProject(persistentProject, updatedProject);
            } else {
                persistentIterator.remove();
                persistentProject.setDeveloper(null);
            }
        }
        persistentProjects.addAll(projectsToAdd);
    }

    private void updateProject(Project persistentProject, Project updatedProject) {
        persistentProject.setMentor(updatedProject.getMentor());
        persistentProject.setDescription(updatedProject.getDescription());
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        developerDao.delete(id);
    }
}
