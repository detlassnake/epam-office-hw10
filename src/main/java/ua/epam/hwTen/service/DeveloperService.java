package ua.epam.hwTen.service;

import ua.epam.hwTen.dto.DeveloperDto;

public interface DeveloperService {
    DeveloperDto create(DeveloperDto dto);
    DeveloperDto get(String passportNumber);
    DeveloperDto update(DeveloperDto dto);
    void delete(Integer id);
}
