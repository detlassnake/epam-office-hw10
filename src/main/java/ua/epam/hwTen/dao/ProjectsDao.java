package ua.epam.hwTen.dao;

import ua.epam.hwTen.entity.Mentor;

import java.util.List;

public interface ProjectsDao {
    Mentor create (String name);
    List<Mentor> getByIds(List<Integer> ids);
    List<Mentor> getAll();
}
