package ua.epam.hwTen.dao;

import ua.epam.hwTen.entity.Mentor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ProjectsDaoImpl implements ProjectsDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Mentor create(String name) {
        Mentor mentor = new Mentor();
        mentor.setName(name);
        sessionFactory.getCurrentSession().save(mentor);
        return mentor;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Mentor> getByIds(List<Integer> ids) {
        Session session = sessionFactory.getCurrentSession();
        List<Mentor> result = session.byMultipleIds(Mentor.class).multiLoad(ids);
        return result;
    }



    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Mentor> getAll() {
        List<Mentor> result = sessionFactory.getCurrentSession().createQuery("from Mentor").list();
        return result;
    }
}
