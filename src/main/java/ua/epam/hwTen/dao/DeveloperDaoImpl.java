package ua.epam.hwTen.dao;

import ua.epam.hwTen.entity.Developer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Repository
public class DeveloperDaoImpl implements DeveloperDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(Developer developer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(developer);
    }

    @Override
    public Developer get(Integer developerId) {
        Session session = sessionFactory.getCurrentSession();
        Developer developer = session.get(Developer.class, developerId);
        return Objects.requireNonNull(developer, "Developer not found by id: " + developerId);
    }

    @Override
    public Developer get(String passportNumber) {
        Session session = sessionFactory.getCurrentSession();
        Object singleResult = session
                .getNamedQuery("developerByPassportNumber")
                .setParameter("passNum", passportNumber)
                .getSingleResult();
        return (Developer) singleResult;
    }

    @Override
    public void delete(Integer id) {
        Developer developer = get(id);
        sessionFactory.getCurrentSession().delete(developer);
    }
}
