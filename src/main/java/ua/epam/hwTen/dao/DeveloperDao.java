package ua.epam.hwTen.dao;

import ua.epam.hwTen.entity.Developer;

public interface DeveloperDao {
    void save(Developer developer);
    Developer get(Integer developerId);
    Developer get(String passportNumber);
    void delete(Integer id);
}
