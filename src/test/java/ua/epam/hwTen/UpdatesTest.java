package ua.epam.hwTen;


import ua.epam.hwTen.cfg.HibernateConfiguration;
import ua.epam.hwTen.dao.ProjectsDao;
import ua.epam.hwTen.dto.ProjectDto;
import ua.epam.hwTen.dto.DeveloperDto;
import ua.epam.hwTen.entity.Mentor;
import ua.epam.hwTen.service.DeveloperService;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
public class UpdatesTest {

    @Autowired
    private DeveloperService developerService;
    @Autowired
    private ProjectsDao projectsDao;

    private static final String[] MENTOR_NAMES = {"Aqvafina", "Camamber", "Jerald", "Segen" };

    private Map<String, Mentor> mentors = Collections.emptyMap();

    @Before
    public void initializeMentors() {
        List<Mentor> initialList = projectsDao.getAll();
        if (initialList.isEmpty()) {
            initialList = createMentors();
        }
        mentors = initialList.stream().collect(Collectors.toMap(Mentor::getName, Function.identity()));

    }


    private List<Mentor> createMentors() {
        List<Mentor> result = Stream
                .of(MENTOR_NAMES)
                .map(name -> projectsDao.create(name))
                .collect(Collectors.toList());
        return result;
    }

    @Test
    public void testCreateDeveloper() {
        DeveloperDto developerDto = createDeveloperDto(RandomString.make(16));

        DeveloperDto createdDeveloper = developerService.create(developerDto);
        Assert.assertEquals(developerDto.getProjects().size(), createdDeveloper.getProjects().size());
    }

    private DeveloperDto createDeveloperDto(String passportNumber) {
        DeveloperDto developerDto = new DeveloperDto();
        developerDto.setPassportNum(passportNumber);
        developerDto.setName("John Smith");

        ProjectDto firstProject = new ProjectDto();
        firstProject.setDescription("First project");
        firstProject.setMentorId(getMentorId(0));
        developerDto.getProjects().add(firstProject);

        ProjectDto secondProject = new ProjectDto();
        secondProject.setDescription("Second project");
        secondProject.setMentorId(getMentorId(1));
        developerDto.getProjects().add(secondProject);
        return developerDto;
    }

    @Test
    public void testGetDeveloper() {
        String passportNumber = RandomString.make(16);
        DeveloperDto developerDto = createDeveloperDto(passportNumber);
        developerService.create(developerDto);
        DeveloperDto getResult = developerService.get(passportNumber);

        Assert.assertEquals(developerDto.getProjects().size(), getResult.getProjects().size());
    }

    @Test
    public void testDeleteDeveloper() {
        String passportNumber = RandomString.make(16);
        DeveloperDto developerDto = createDeveloperDto(passportNumber);
        DeveloperDto createdDeveloper = developerService.create(developerDto);
        developerService.delete(createdDeveloper.getId());
    }

    @Test
    public void testUpdateDeveloper() {
        String passportNumber = RandomString.make(16);
        DeveloperDto developerDto = createDeveloperDto(passportNumber);
        DeveloperDto createdDeveloper = developerService.create(developerDto);

        ProjectDto firstProject = createdDeveloper.getProjects().get(0);
        firstProject.setDescription("New description");
        firstProject.setMentorId(getMentorId(2));

        createdDeveloper.setName("new name");
        createdDeveloper.getProjects().remove(1);


        ProjectDto newProject = new ProjectDto();
        newProject.setDescription("Another desc");
        newProject.setMentorId(getMentorId(3));

        createdDeveloper.getProjects().add(newProject);
        DeveloperDto updated = developerService.update(createdDeveloper);
    }

    private Integer getMentorId(int mentorIndex) {
        return mentors.get(MENTOR_NAMES[mentorIndex]).getId();
    }

}
